Bootstrap Cloud
===============

A Drupal [Bootstrap 5](https://drupal.org/project/bootstrap5) based sub-theme.
Designed for [Cloud](https://drupal.org/project/cloud) module

License
=======

Cloud Orchestrator is released under the terms of the [GPLv2 license](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html#SEC1).

Maintainers
===========

- `Yas Naoi (yas)` (https://drupal.org/u/yas)
- `Baldwin Louie (baldwinlouie)` (https://www.drupal.org/u/baldwinlouie)
