/**
 * @file
 * Functions for supporting tertiary navigation.
 */
(function ($) {
  'use strict';
  Drupal.behaviors.menuBehavior = {
    attach: function () {
      $('.navbar a.dropdown-toggle').on('click', function (e) {
        var $el = $(this);
        var $parent = $(this).offsetParent('.dropdown-menu');
        $(this).parent('li').toggleClass('open');

        if (!$parent.parent().hasClass('nav')) {
          $el
            .next()
            .css({ top: $el[0].offsetTop, left: $parent.outerWidth() - 4 });
        }
        $('.nav li.open').not($(this).parents('li')).removeClass('open');
        return false;
      });

      $(document).click(function (e) {
        var target = e.target;
        if (
          !$(target).is('.navbar a.dropdown-toggle, .navbar .navbar-toggler') &&
          !$(target)
            .parents()
            .is('.navbar a.dropdown-toggle, .navbar .navbar-toggler')
        ) {
          $('.dropdown').removeClass('open');
          $('.navbar .navbar-collapse').css('display', 'none');
        }
      });

      // Mobile Menu //
      $('.navbar .navbar-collapse').hide();
      $('.navbar .navbar-toggler').on('click', function (e) {
        $('.navbar .navbar-collapse').slideToggle('slow');
      });
    }
  };
})(jQuery);
