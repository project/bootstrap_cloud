/**
 * Provide Bootstrap navbar preview.
 */
/**
 * @file
 * Functions for supporting tertiary navigation.
 */
(function ($) {
  'use strict';

  Drupal.behaviors.bootstrapNavbarPreview = {
    attach: function (context) {
      var $context = $(context);
      var $preview = $context.find('#edit-navbar');
      $preview.once('navbar').each(function () {
        var $body = $context.find('body');
        var $navbar = $context.find('#navbar .navbar');
        $preview
          .find('select[name="navbar_position"]')
          .bind('change', function () {
            var $position = $(this).find(':selected').val();
            $navbar.removeClass(
              'navbar-fixed-bottom navbar-fixed-top navbar-static-top container'
            );
            if ($position.length) {
              $navbar.addClass('navbar-' + $position);
            } else {
              $navbar.addClass('container');
            }
            // Apply appropriate classes to body.
            $body.removeClass(
              'navbar-is-fixed-top navbar-is-fixed-bottom navbar-is-static-top'
            );
            switch ($position) {
              case 'fixed-top':
                $body.addClass('navbar-is-fixed-top');
                break;

              case 'fixed-bottom':
                $body.addClass('navbar-is-fixed-bottom');
                break;

              case 'static-top':
                $body.addClass('navbar-is-static-top');
                break;
            }
          });
        $preview
          .find('input[name="navbar_inverse"]')
          .bind('change', function () {
            $navbar.toggleClass('navbar-inverse navbar-default');
          });
      });

      $('.custom-dropdown .dropdown-menu li').each(function () {
        var $icon = $(this).find('span').clone();
        $(this).find('span').remove();
        $(this).find('a').prepend($icon);
      });

      $("form .form-actions input[value='Apply to selected items']")
        .once()
        .each(function () {
          $(this).wrap("<div class='btn btn-info btn-wrap apply-btn' />");
        });

      $('form .form-actions .apply-btn')
        .once()
        .each(function () {
          $(this).prepend(
            '<span class="btnapply glyphicon glyphicon-ok"></span>'
          );
        });

      $('form .form-date, form .form-time').addClass('form-control');

      $('.aws_cloud_instance_type_prices')
        .parent()
        .addClass('custom-table-head');
      $(
        '.custom-table-head .aws_cloud_instance_type_prices th a, .custom-table-head .sticky-header th a'
      ).click(function () {
        location.reload();
      });

      $('table th.is-active a[href*="sort=desc"]').each(function () {
        $('table th.is-active a').append(
          '<span class="caret downarrow"></span>'
        );
      });
      $('table th.is-active a[href*="sort=asc"]').each(function () {
        $('table th.is-active a').append('<span class="caret"></span>');
      });

      $('button.close').on('click', function () {
        $(this).parent().hide();
      });

      // Tab js
      $('.tabbable-panel ul.nav-tabs a').click(function () {
        // Check for active
        $('.tabbable-panel ul.nav-tabs li').removeClass('active');
        $(this).parent().addClass('active');

        // Display active tab
        let currentTab = $(this).attr('href');
        $('.tabbable-panel .tab-content div.tab-pane').hide();
        $(currentTab).show();

        return false;
      });

      var tooltipTriggerList = [].slice.call(
        document.querySelectorAll('[data-bs-toggle="tooltip"]')
      );
      var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
      });
    }
  };
})(jQuery);
