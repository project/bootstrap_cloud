/**
 * @file
 * Provides an event handler for hidden elements in dropdown menus.
 * Do not need this file anymore.  This is solved using CSS.
 */

(function ($) {
  $('.table-responsive').on('show.bs.dropdown', function () {
    $('.table-responsive').css('overflow', 'inherit');
  });

  $('.table-responsive').on('hide.bs.dropdown', function () {
    $('.table-responsive').css('overflow', 'auto');
  });

  $(document).on('click', '.custom-dropdown > button', function (event) {
    event.stopPropagation();

    // If there is no caret class, and the button is clicked,
    // redirect the user to the child link.
    if (!$(this).find('span').hasClass('caret')) {
      if ($(this).find('a').length > 0) {
        window.location.href = $(this).find('a').attr('href');
      }
    }
    $('.custom-dropdown .dropdown-menu').hide();
    if ($(this).hasClass('open')) {
      $(this).next().hide();
      $(this).removeClass('open');
    } else {
      $('.custom-dropdown > button').removeClass('open');
      $(this).addClass('open');
      $('.custom-dropdown .dropdown-menu').hide();
      $(this).next().show();
    }
    $('.custom-dropdown .dropdown-menu li').each(function () {
      var $icon = $(this).find('span').clone();
      $(this).find('span').remove();
      $(this).find('a').prepend($icon);
    });
  });

  $(document).click(function (e) {
    var target = e.target;
    if (
      !$(target).is('.custom-dropdown > button') &&
      !$(target).parents().is('.custom-dropdown > button')
    ) {
      $('.custom-dropdown .dropdown-menu').hide();
    }
  });
})(jQuery);
